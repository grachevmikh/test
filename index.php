<?php
	include_once('db.php');
	
	function blog($page = 1){
		$DB = new DB();
		$limit = 3; //колво записей на страницу
		$page = $page;
		$start = ($page - 1) * $limit;
		$blog = $DB->query("SELECT * FROM `blog` ORDER BY date DESC LIMIT " . $start . "," .$limit);
		return $blog;
	}
	if(isset($_POST['p'])){
		$post = blog(intval($_POST['p']));
		echo json_encode($post);
 		die();
	}else{
		$post = blog();
	}
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Тестовое задание" />
		<meta name="keywords" content="" />
		<title>Тестовое задание</title>
	</head>
	<body>
		<header>
			<h1>Тестовое задание</h1>
		</header>
		<content id="data-container">
			<?php if(!empty($post)) {
						for ($i = 0; $i < count($post); $i++) { ?>
			<div>
				<h2><?php echo $post[$i]->title;?></h2>
				<div>
					<img src="<?php echo $post[$i]->image;?>" alt=""><br>
					<?php echo $post[$i]->description;?>
				</div>
				<div>
					Дата:<?php echo $post[$i]->date;?>
				</div>
			</div>
			<?php 		}
					} else { echo "Нет записей";} ?>
		</content>
		<button id="button-more">Показать еще</button>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
		$('document').ready(function(){
			var page = 2;
		
			$('#button-more').click(function (){
				$.ajax({
					url:"index.php",
					type:"POST",
					dataType:'json',
					data:{p:page},
					success: function(data){
						var post = "";
						$.each(data,function(key, value){
							post += "<div><h2>" + value['title'] +"</h2><div><img src='" + value['image'] +"' alt=''><br>" + value['description'] +"</div><div>Дата:" + value['date'] +"</div></div>"
						});
						$(post).appendTo('#data-container');
						if(data.length == 3){
							page++;
						}else{
							$('#button-more').hide();
						}
					}
				});
			});
		});
		</script>
	</body>
</html>
