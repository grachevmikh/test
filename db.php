<?php 
// класс для работы с базой данных через PDO

class DB { 
	
	private $db;
	// подключение к бд
	function __construct() {
		try {
			$this->db = new PDO('mysql:host=localhost;dbname=test',"admin", "passwd");
			$this->db->exec("SET CHARACTER SET utf8");
		} catch (PDOException $error) {
			echo 'Error : '.$error->getMessage();
		}
	}
	
	//функиця для выборки данных из бд
	public function query($sql) {
		if(isset($this->db)) {
			$result = $this->db->query($sql);
			$result = $result->FetchAll(PDO::FETCH_OBJ);
			return $result;
		}
		return false;
	}
} 
